"""
Test for hamming_metric
"""

import unittest

from src.hamming_metric import hamming_metric, i_am_your_father


class HammingTest(unittest.TestCase):

    def test_hamming(self):
        test_dna1 = 'GCTAC'
        test_dna2 = 'CCTAT'

        self.assertEqual(hamming_metric(test_dna1, test_dna2), 2)
        self.assertEqual(hamming_metric(['A', 'B'], ['B', 'A']), 2)

    def test_whos_father_raises_value_error(self):
        fathers_list = ['AAAGCT', 'GCTAAA']
        child_dna = 'GCTGCT'

        with self.assertRaises(ValueError):
            i_am_your_father(fathers_list, child_dna)

    def test_whos_father(self):
        fathers_list = ['AAAGCT', 'GCTAAC', 'GCTGCT']
        child_dna = 'GCTGCT'

        result = i_am_your_father(fathers_list, child_dna)

        self.assertEqual(result, 2)
