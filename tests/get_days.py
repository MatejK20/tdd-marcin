import unittest

from parameterized import parameterized

from src.get_days import find_day


class GetDaysTest(unittest.TestCase):
    @parameterized.expand(
        [
            ['Test1', 2019, 1, 6, 6],
            ['Test1', 2019, 10, 19, 292],
        ]
    )
    def test_parametrized(self, name, a, b, c, d):
        self.assertEqual(find_day(a, b, c), d, name)
