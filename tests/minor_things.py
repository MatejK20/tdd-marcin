"""
Test minor things
"""

import unittest

from is_leap_year import LeapTestCase


def minor_things_test_suite():
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    suite.addTest(
        loader.loadTestsFromTestCase(LeapTestCase)
    )

    suite.addTest(
        loader.loadTestsFromName(
            'tests.fizzbuzz.FizzBuzzTest'
        )
    )

    suite.addTest(
        loader.loadTestsFromName(
            'tests.BankTestCase'
        )
    )

    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(minor_things_test_suite())