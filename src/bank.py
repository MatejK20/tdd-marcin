"""
Account and Card classes
"""
import random

class Account:

    def __init__(self, first, last, number, balance):
        #self._number = random.randint(99999, 100000)
        self.first = first
        self.last = last
        self._number = number
        self._balance = balance

    @property
    def owner(self):
        return f'{self.first} {self.last}'

    @property
    def balance(self):
        return self._balance

    @property
    def number(self):
        return self._number

    def transfer(self, amount):
        if self.balance + amount <= 0:
            raise  ValueError('Too low account balance')
        self._balance += amount

    def __str__(self):
        return f'Account(number={self._number})'

class Card:

    def __init__(self, account, pin):
        self._account = account
        self._pin = pin

    @property
    def account(self):
        return self._account

    def check_pin(self, pin):
        return self._pin == pin

    def __str__(self):
        return f'{self.account.owner}'

class ATM:

    def withdraw(self, card, pin, amount):
        if not card.check_pin(pin):
            raise ValueError('Pin incorrect')

        card.account.transfer(-amount)